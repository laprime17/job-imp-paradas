﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
using System.Runtime.ConstrainedExecution;
using System.Security;
using static Windows_Jobs1.Impersonation;

namespace Windows_Jobs1.Jobs
{
    public class ObtenerParadasJob : Quartz.IJob
    {
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
        int dwLogonType, int dwLogonProvider, out SafeTokenHandle phToken);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        public class parada
        {
            public Int64 id { get; set; }
            public String turno { get; set; }
            public String equipo { get; set; }
            public DateTime momento { get; set; }
            public Int32 duracion_seg { get; set; }
            public String estado { get; set; }
            public String comentarios { get; set; }
            public String razon { get; set; }
            public Int16 codigo { get; set; }
            public String categoria { get; set; }
        }
   
        public Task Execute(IJobExecutionContext context)
        {
            SafeTokenHandle safeTokenHandle;

            const int LOGON32_PROVIDER_DEFAULT = 0;
            //This parameter causes LogonUser to create a primary token.
            const int LOGON32_LOGON_INTERACTIVE = 2;

            bool returnValue = LogonUser("PELAB.ReportingMantS", ".\\", "t\"3b4Bq - g7JZ",LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, out safeTokenHandle);

            using (WindowsIdentity newId = new WindowsIdentity(safeTokenHandle.DangerousGetHandle()))
            {
                using (WindowsImpersonationContext impersonatedUser = newId.Impersonate())
                {

                    // Check the identity.
                    Console.WriteLine("After impersonation: "
                        + WindowsIdentity.GetCurrent().Name);
             
                    //using (new Impersonation(".\\", "usersql", "Ac123"))
                    //{

                    string masterfile = ConfigurationManager.AppSettings["masterfile"];
                    string logfile = ConfigurationManager.AppSettings["logfile"];
                    string masterfolder = ConfigurationManager.AppSettings["masterfolder"];
                    string name_back = masterfolder + "paradas_" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString()+".csv";
                    File.Create(name_back).Dispose();
                    int nro_registros = 0;
                    using (StreamWriter writer = new StreamWriter(logfile, true))
                    {writer.WriteLine("Fecha y hora: " + DateTime.Now);}
                    String keycarga = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                    try
                    {
                        using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["sc_sirepc"].ConnectionString))
                        {
                            connection.Open();
                            String queryString = @"SELECT  top 3 ShiftStartDate ,FullShiftSuffix,FullShiftName,ShiftStartTimestamp,ShiftId into #Turnos FROM  MLABOperational.Common.ShiftInfo
                         ORDER BY ShiftStartTimestamp DESC
                        declare @idturno_ini int = (select MIN(ShiftId) from #Turnos)
                        declare @idturno_fin int = (
                						        select  max(ShiftId) from
                						        (SELECT ShiftId from #Turnos
                						        except 
                						        select max(ShiftId)ShiftId from #Turnos)G)

                        select 	ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) AS  id,	
                		         si.FullShiftName turno,	
                		         a.FieldId equipo,
                	         MLABOperational.Common.GetTSBySecs(si.ShiftStartTimestamp + CASE 
                													         WHEN a.FieldTime > a.ShiftEndTime THEN a.ShiftEndTime
                													         ELSE a.FieldTime END) momento,
                		         ISNULL(b.FieldTime,a.ShiftEndTime) - CASE
                										          WHEN a.FieldTime > a.ShiftEndTime THEN a.ShiftEndTime
                										          ELSE a.FieldTime
                										         END duracion_seg,
                		         a.Status estado,
                		         a.FieldComment comentarios,
                		         a.FieldName razon,
                		         a.FieldReason codigo,
                		         a.Category categoria
                        from MLABOperational.rpt.[EquipmentStatus](@idturno_ini,@idturno_fin) a
                         inner join #Turnos si
                         on
                	         a.ShiftId = si.ShiftId
                        left join MLABOperational.rpt.[EquipmentStatus](@idturno_ini,@idturno_fin) b
                         on
                	         a.ShiftId = b.ShiftId
                	         and a.fieldid = b.FieldId
                	         and a.ROWID = b.ROWID - 1
                         left join 
                        (select * from MLABOperational.Common.EnumUNIT where Description in ('Camion','Pala','Perforadora','Motoniveladora','Tractor Orugas','Tractor Ruedas'))u
                         on
                         u.Id = a.FieldUnit
                         where
                	         u.[Description] in ('Pala','Perforadora','Camion')
                	         and a.Status IN ('Malogrado','Disponible','Standby','Demora')	
                         order by
                	         u.[Description],
                	         a.FieldId,
                	         momento;";

                            queryString = "select top 50 *  from EVE.eventosEquipo_dispatch";


                            SqlCommand cmd = new SqlCommand(queryString, connection);
                            SqlDataReader dr = cmd.ExecuteReader();
                    
                            using (System.IO.StreamWriter fs = new System.IO.StreamWriter(masterfile))
                            {
                                // Loop through the fields and add headers
                                for (int i = 0; i < dr.FieldCount; i++)
                                {
                                    string name = dr.GetName(i);
                                    if (name.Contains(","))
                                        name = "\"" + name + "\"";
                               
                                    fs.Write(name + ",");
                                }
                                fs.WriteLine();

                                // Loop through the rows and output the data
                                while (dr.Read())
                                {
                                    for (int i = 0; i < dr.FieldCount; i++)
                                    {
                                        string value = dr[i].ToString();
                                        if (value.Contains(","))
                                            value = "\"" + value + "\"";

                                        fs.Write(value + ",");
                                    }
                                    fs.WriteLine();
                                    nro_registros++;

                                }

                                fs.Close();
                            }
                            using (System.IO.StreamWriter fs = new System.IO.StreamWriter(name_back))
                            {
                                // Loop through the fields and add headers
                                for (int i = 0; i < dr.FieldCount; i++)
                                {
                                    string name = dr.GetName(i);
                                    if (name.Contains(","))
                                        name = "\"" + name + "\"";

                                    fs.Write(name + ",");
                                }
                                fs.WriteLine();

                                // Loop through the rows and output the data
                                while (dr.Read())
                                {
                                    for (int i = 0; i < dr.FieldCount; i++)
                                    {
                                        string value = dr[i].ToString();
                                        if (value.Contains(","))
                                            value = "\"" + value + "\"";

                                        fs.Write(value + ",");
                                    }
                                    fs.WriteLine();
                                }

                                fs.Close();
                            }

                        }
                        using (StreamWriter writer = new StreamWriter(logfile, true))
                        {
                            writer.WriteLine("Acceso correcto a BD MCR. Nro de registros: "+nro_registros.ToString());
                            writer.WriteLine("Acceso correcto con usuario: " + WindowsIdentity.GetCurrent().Name);
                            
                        }

                        using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["sc_sirepc"].ConnectionString))
                        {

                            var cmd = new SqlCommand("MCR.mcr_populate_daily_events", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@keycarga", keycarga));
                            conn.Open();
                            cmd.ExecuteNonQuery();
                        }
                    }
                    catch (Exception e)
                    {
                        using (StreamWriter writer = new StreamWriter(logfile, true))
                        { writer.WriteLine("ERROR: " + e.Message.ToString()); }
                    }
       
                    using (StreamWriter writer = new StreamWriter(logfile, true))
                    { writer.WriteLine("-----------"); }

                    return Task.FromResult(0);
                }
            }
        }
        //}
    }
}
