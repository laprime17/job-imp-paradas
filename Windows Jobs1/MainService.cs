﻿using Autofac;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Windows_Jobs1.Jobs;
using Zetalex.Windows_Jobs1.Helpers;

namespace Zetalex.Windows_Jobs1
{
    public partial class MainService : ServiceBase
    {
        private NLog.Logger Logger;
        private IScheduler scheduler;

        public MainService()
        {
            InitializeComponent();

            Logger = NLog.LogManager.GetCurrentClassLogger();
        }

        public object CronScheduleBuilder { get; private set; }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected async override void OnStart(string[] args)
        {
            try
            {
                //Configure Container
                IContainer container = Helpers.IocConfig.ConfigureContainer();

                //Configure LogProvider
                //Quartz.Logging.LogProvider.SetCurrentLogProvider(new Logging.NLogProvider());
                Quartz.Logging.LogProvider.SetCurrentLogProvider(new Logging.ConsoleLogProvider());

                //StdSchedulerFactory factory = new StdSchedulerFactory();
                ISchedulerFactory factory = container.Resolve<ISchedulerFactory>();
                scheduler = await factory.GetScheduler();

                //Start scheduler
                await scheduler.Start();

                //Get All Jobs
                var lstJobs = JobFactory.GetAllJobs();
                JobKey jobKey = JobKey.Create("ObtenerParadasJob");
                foreach (var jobItem in lstJobs)
                {
                    //Define the job 
                    IJobDetail job = JobBuilder.Create<ObtenerParadasJob>().WithIdentity(jobKey).Build();

                    //Trigger the job to run now, and then repeat by Interval
                    ITrigger trigger = null;
                   
                        trigger = TriggerBuilder.Create()
                            .WithIdentity("JobTrigger")
                            .StartNow()
                            .WithSchedule(Quartz.CronScheduleBuilder.CronSchedule(new CronExpression("*/30 * * * * ?")))
                            //.WithSchedule(Quartz.CronScheduleBuilder.DailyAtHourAndMinute(23, 40))
                            .Build();
                    

                    //Tell quartz to schedule the job using our trigger
                    await scheduler.ScheduleJob(job, trigger);
                }
            }
            catch (SchedulerException se)
            {
                Console.WriteLine(se);
            }

            Logger.Info("Service Started");
        }
        protected async override void OnStop()
        {
            //Shut down the scheduler when you are ready to close your program
            await scheduler.Shutdown();

            //Write scheduler MetaData
            SchedulerMetaData metaData = await scheduler.GetMetaData();
            Logger.Info("Executed " + metaData.NumberOfJobsExecuted + " jobs.");

            Logger.Info("Service Stopped");
        }
    }
}
